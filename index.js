/*
	Do the following in the terminal after creating the index.js
	
	- Create a "package.json" file
		npm init -y 

	- Install Express JS and Mongoose.
		npm install express mongoose

*/




const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@zuittbootcamp.e1kfs3u.mongodb.net/b183_to-do?retryWrites=true&w=majority",{useNewUrlParser: true,useUnifiedTopology: true});
// Set a notification for connection success or error with our database.
let db = mongoose.connection;

// if a connection error occurred, it will be output in the console.
// console.error.bind(console) allows us to print therror in the browser consoles and in the terminal.
db.on("error",console.error.bind(console,"connection error"))

// if the connection is successful, a console message will be shown.
db.once("open", () =>console.log("We're connected to the cloud database."))

// Mongoose Schema
// Determine the structure of the document to be written in the database
// Schemas act as blueprints to our data.
const taskSchema = new mongoose.Schema({
	// Name of the task
	name: String, //String is a shorthand for name:{type: String}
	// Status task (Completed, Pending, Incomplete)
	status:{
		type: String,
		// Default values are the predefined values for a field if we don't put any value.
		default: "Pending"
	}

});
// Mongoose Model
// Model uses Schemas and they act as the middleman from the server (JS code) to our database.
/*
	Syntax:
	const modelName = mongoose.model("collectionName", mongooseSchema);
*/
	// Model must be in singular form and Capitalize the first letter.
	// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection.
	const Task = mongoose.model("Task", taskSchema);
const userSchema = new mongoose.Schema({
	name: String,
	password: String
})

const User = mongoose.model("User", userSchema);
// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Route for creating a task
// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task doesn't exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

	Task is duplicated if:
	- result from the query not equal to null
	- req.body.name is equal to result.name.
*/

app.post("/tasks", (req,res)=>{
	// Call back functions in mongoose methods are program this way:
		// first parameter store the error
		// second parameter return the result.
	Task.findOne({name:req.body.name},(err,result)=>{
		console.log(result);

		if(result != null&& req.body.name == result.name){
			// Return a message to the client/postman
			return res.send("Duplicate task found!");
		}
		// If no document was found or no duplicate.
		else{
			// Create a new task object and save to database
			let newTask = new Task({
				name: req.body.name
			});
			// The ".save" method will store the information to the  database
			// Since the "newTask" was created/instantiated from the Task model that contains the Mongoose Schema, so it will gain access to the save method.

			newTask.save((saveErr, saveTask)=>{
				// If there are errors in saving it will be displayed in the console.
				if(saveErr){
					return console.error(saveErr);
				}
				// If no error found while creating the document it will be save in the database
				else{
					return res.status(201).send("New Tasks created.");
				}
			})
		}
	})

})

// Business Logic
		/*
		1. Retrieve all the documents
		2. If an error is encountered, print the error
		3. If no errors are found, send a success status back to the client/Postman and return an array of documents
		*/

app.get("/tasks", (req,res)=>{
	Task.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}
		else{
			 return res.status(200).send(result);
		}
	})
})

app.get("/user", (req,res)=>{
	User.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}
		else{
			 return res.status(200).send(result);
		}
	})
})
app.post("/user",(req,res)=>{
	if(req.body.userName!="" && req.body.password!=""){
		User.findOne({userName: req.body.userName}, (err,result)=>{
			console.log(result);
			if(result != null && req.body.userName == result.userName){
				return res.send("Duplicate task found");
			}
			else{
				let newUser = new User({
					userName: req.body.userName,
					password: req.body.password
				})
				newUser.save((saveErr, saveTask)=>{
					if(saveErr){
						return console.log(saveErr);
					}
					else{
						return res.status(201).send("New user created");
					}
				})
			}
		})
	}
	else{
		return res.send("Username and Password are required");
	}
})

app.listen(port,() => console.log(`Server running at port ${port}`));